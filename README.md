# Advice App

This is a simple starter API for a Advice Application

## Getting Started

1. Clone or Download the Repo
2. cd /advice-app
3. Run "npm install" at the command prompt

## Running API

1. Run "npm start" at the command line
2. Access the api via the following urls

	http://localhost:3000/ - for random advice

	http://localhost:3000/info - for developer information
	
	http://localhost:3000/health - to verify API is healthy